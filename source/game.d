﻿module game;

import std.stdio, std.algorithm, std.string, std.random;

import Dgame.Graphics.all;
import Dgame.Math.all;
import Dgame.System.all;
import Dgame.Window.all;

import keyboard;

class Game
{
	//region Fields
	Window window;
	Text scoreText;
	private Clock clock;
	private uint total, elapsed, prevTotal;
	private uint score, catched, miss, alls;
	private float platformVelocity = .7f;
	private float velocity = 0.3f;
	private alias win = window;
	bool isPause = true;
	private Text pause;

	Shape platform;
	Shape[] shapes;
	//endregion

	this(string title)
	{
		window = new Window(VideoMode(480, 480), title);
		window.setClearColor(Color.Black);
		scoreText = new Text(Font("arial.ttf", 14), "Score: 0");
		scoreText.setBackgroundColor(Color.Black);
		scoreText.setColor(Color.White);
		pause = new Text(Font("arial.ttf", 14), "PAUSE");
		pause.setBackgroundColor(Color.Black);
		pause.setColor(Color.White);
		pause.setPosition(240, 240);
		platform = Shape.make(Shape.Type.Quad, [Vertex(0, 0), Vertex(32 * 3, 0), Vertex(32 * 3, 32), Vertex(0, 32)]);
		platform.setColor(Color.Green);
		platform.setPosition(240 - 32 * 1.5, 480 -16);
	}

	void update()
	{
		static uint counter;

		debug win.setTitle(format("Catch Me! FPS: %s", clock.getCurrentFps()));

		counter += elapsed;

		if(miss >= 3)
		{
			score = 0;
			catched = 0;
			miss = 0;
			velocity = 0.3f;
			isPause = true;
		}

		switch(catched)
		{
			case 10:
				velocity = 0.35f;
				break;
			case 25:
				velocity = 0.45f;
				break;
			case 50:
				velocity = 0.55f;
				break;
			case 100:
				velocity = 0.65f;
				break;
			case 200:
				velocity = 0.95f;
				break;
			default:
				break;
		}

		if(counter >= 1000)
		{
			auto obj = Shape.make(Shape.Type.Quad, [Vertex(0, 0), Vertex(32, 0), Vertex(32, 32), Vertex(0, 32)]);
			obj.setPosition(32 * uniform(0, 14), 0);
			obj.setColor(Color.Red);
			shapes ~= obj;
			counter = 0;
		}
		foreach(i ,ref Shape obj; shapes)
		{
			if(isCatch(obj))
			{
				score += 100 * alls * (velocity + 1);
				shapes = std.algorithm.remove(shapes, i);
				debug writeln("Catched!");
				catched++;
				alls++;
				continue;
			}
			if(isMiss(obj))
			{
				shapes = std.algorithm.remove(shapes, i);
				debug writeln("Missed!");
				miss++;
				alls = 0;
				continue;
			}
			auto pos = obj.getPosition();
			obj.setPosition(pos.x, pos.y + velocity * elapsed);
		}
		scoreText.format("Score: %s Поймано: %s Пропущенно: %s", score, catched, miss);
	}

	void draw()
	{
		foreach(obj; shapes)
			win.draw(obj);
		win.draw(platform);
		win.draw(scoreText);
		if(isPause)
			win.draw(pause);
	}

	void isKeyDown()
	{
		alias Key = Keyboard.Code;
		auto pos = platform.getPosition();
		if(KeyState.isKeyPressed(Key.Right))
		{
			auto x = pos.x + platformVelocity * elapsed;
			if(x > 480 - 32*3) x = 480 - 32*3;
			platform.setPosition(x, pos.y);
		}
		if(KeyState.isKeyPressed(Key.Left))
		{
			auto x = pos.x - platformVelocity * elapsed;
			if(x < 0) x = 0;
			platform.setPosition(x, pos.y);
		}
	}

	void loop()
	{
		Event event;
		bool isKeyPressed;
		while(win.isOpen)
		{
			total = clock.getTicks();
			elapsed = total - prevTotal;
			prevTotal = total;
			while(EventHandler.poll(&event))
			{
				alias Type = Event.Type;
				switch(event.type)
				{
					case Type.Quit:
						win.close();
						break;
						/*
					case Type.KeyDown:
						if(event.keyboard.key == Keyboard.Code.Space)
							isPause = !isPause;
						break;
						*/
					default:
						break;
				}
			}
			
			if(KeyState.isKeyDown(Keyboard.Code.Space))
				isPause = !isPause;

			if(!isPause)
			{
				isKeyDown();
				update();
			}

			KeyState.update();

			win.clear();
			draw();
			win.display();
			isKeyPressed = false;
		}
	}

	bool isCatch(Shape obj)
	{
		Vector2!float pos = platform.getPosition();
		Vector2!float o_pos = obj.getPosition();

		bool x = o_pos.x > pos.x - 32 && o_pos.x < pos.x + 32 * 4;
		bool y = o_pos.y >= 480 - 32;
		return x && y;
	}

	bool isMiss(Shape obj)
	{
		Vector2!float pos = platform.getPosition();
		Vector2!float o_pos = obj.getPosition();
		
		bool x = o_pos.x > pos.x - 32 && o_pos.x < pos.x + 32 * 4;
		bool y = o_pos.y >= 480 - 32;
		return !x && y;
	}
}