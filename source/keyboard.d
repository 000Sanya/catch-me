﻿module keyboard;

import std.algorithm, std.stdio;

import Dgame.System.Keyboard;

final abstract class KeyState
{
	alias Code = Keyboard.Code;
	private
	{
		static ubyte* currentKeyState = null;
		static ubyte[] prevKeyState;
	}

	public static this()
	{
		currentKeyState = Keyboard.getState();
		prevKeyState = new ubyte[513];
	}

	static void update()
	{
		ubyte[] temp = currentKeyState[0..512];
		copy(temp, prevKeyState);
	}

	static bool isKeyDown(Code code)
	{
		int t = Keyboard.toScanCode(code);
		debug writefln("Prev: %s, Current: %s", prevKeyState[t], currentKeyState[t]);
		auto i = !prevKeyState[t] && currentKeyState[t];
		if(i)
			return true;
		return false;
	}

	static bool isKeyPressed(Code code)
	{
		int t = Keyboard.toScanCode(code);
		debug writefln("Prev: %s, Current: %s", prevKeyState[t], currentKeyState[t]);
		if(prevKeyState[t] && currentKeyState[t])
			return true;
		return false;
	}

	static bool isKeyUp(Code code)
	{
		int t = Keyboard.toScanCode(code);
		debug writefln("Prev: %s, Current: %s", prevKeyState[t], currentKeyState[t]);
		if(prevKeyState[t] && !currentKeyState[t])
			return true;
		return false;
	}

	static bool isKeyReleased(Code code)
	{
		int t = Keyboard.toScanCode(code);
		debug writefln("Prev: %s, Current: %s", prevKeyState[t], currentKeyState[t]);
		if(!prevKeyState[t] && !currentKeyState[t])
			return true;
		return false;
	}
}